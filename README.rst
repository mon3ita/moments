Moments
------------

A sample app made with Django.

Try it
************

>>> clone the repository
>>> cd moments
>>> python -m venv venv

``Windows``

>>> venv\Scripts\activate.bat

``Unix/MacOS``

>>> source venv/bin/activate

>>> python run.py

To Do
*****

*   Remove  images
*   Edit Galleries
*   User profiles
*   Login required

Done
****

*   Add/Remove Gallery
*   Insert images

LICENSE
*******

MIT