import os

try:
    with open(".user", "r") as file:
        user = file.readlines()[0].strip()
except:
    user = ""

if not user.lower() in ["y", "yes"]:
    user = input("Did you activate a venv environment? ").strip()
    if user.lower() in ["y", "yes"]:
        requirements = None
        with open("requirements.txt", "r") as file:
            requirements = file.readlines()

        requirements = " ".join([requirement.strip() for requirement in requirements])
        os.system(f"pip install {requirements}")
        with open(".user", "w") as file:
            file.write("y")
        os.system("python moments/manage.py makemigrations")
        os.system("python moments/manage.py migrate")

import sys

try:
    choice = sys.argv[1]

    if choice in ["mg", "migrate"]:
        os.system("python moments/manage.py migrate")
    if choice in ["mkg", "makemigration", "makemigrations"]:
        os.system("python moments/manage.py makemigrations")
except:
    pass

if len(sys.argv) == 1:
    os.system("python moments/manage.py runserver")
