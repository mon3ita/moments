from django.shortcuts import render, redirect
from django.contrib import messages

from . import models
from . import forms

def all(request):
    galleries = models.Gallery.objects.filter(private=False).order_by("-pub_date")
    return render(request, "gallery/index.html", {"galleries": galleries})


def mine(request):
    user_id = request.user.id
    galleries = models.Gallery.objects.filter(user_id=user_id)
    return render(request, "gallery/index.html", {"galleries": galleries})


def get(request, user_id, gallery_id):
    gallery = models.Gallery.objects.get(pk=gallery_id)
    return render(request, "gallery/show.html", {"gallery": gallery.image_set.all()})


def user(request, user_id):
    galleries = models.Gallery.objects.filter(user_id=user_id, private=False).order_by(
        "-pub_date"
    )
    return render(request, "gallery/user.html", {"galleries": galleries})


def create(request):
    user = request.user.id
    form = forms.GalleryForm(request.POST)

    if request.method == "POST":
        if form.is_valid():
            name = request.POST.get('name')
            description = request.POST.get('description')
            private = request.POST.get('private')
            gallery = models.Gallery(name=name, description=description, user_id=user)
            gallery.save()

            if request.FILES.get('front_image'):
                image = models.Image(gallery_id=gallery.id, image=request.FILES.get('front_image'))
                image.save()
            return redirect('mine')
        else:
            for reason, msg in form.errors.items():
                messages.error(request, f"{reason}: {msg[0]}")
    else:
        form = forms.GalleryForm()
    return render(request, "gallery/create.html", {"form": form})


def insert(request, gallery_id):
    if request.method == "POST":
        form = forms.ImageForm(request.POST)
        if request.FILES.get('image'):
            image = models.Image(image=request.FILES.get('image'), description=request.POST.get('description'), gallery_id=gallery_id)
            image.save()
            messages.success(request, "Successfully inserted a new image.")
            return redirect('get', user_id=request.user.id, gallery_id=gallery_id)
        else:
            for reason, msg in form.errors.items():
                messages.error(request, f"{reason}: {msg[0]}")
    else:
        form = forms.ImageForm()

    return render(request, "gallery/insert.html", {"form": form})


def remove(request, user_id, gallery_id):
    gallery = models.Gallery.objects.get(pk=gallery_id, user_id=user_id)
    if gallery:
        gallery.delete()
        return redirect('mine')
    else:
        messages.error(request, f"Album with id {gallery_id} doesn' exist.")
        return redirect('mine')

def edit(request, user_id, gallery_id):
    pass