# Generated by Django 3.0.2 on 2020-01-14 05:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("gallery", "0002_auto_20200113_1731"),
    ]

    operations = [
        migrations.AddField(
            model_name="gallery",
            name="private",
            field=models.BooleanField(default=False),
        ),
    ]
