from django.contrib import admin
from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
    path("", views.all, name="all"),
    path("mine", views.mine, name="mine"),
    path("<int:user_id>", views.user, name="user_gallery"),
    path("<int:user_id>/<int:gallery_id>", views.get, name="get"),
    path("<int:user_id>/<int:gallery_id>/delete", views.remove, name="remove_album"),
    path("<int:user_id>/<int:gallery_id>/edit", views.edit, name="edit_album"),
    path("create", views.create, name="create"),
    path("<int:gallery_id>/insert", views.insert, name="insert"),
]
