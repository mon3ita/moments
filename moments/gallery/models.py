from django.db import models
from django.contrib.auth.models import User

from datetime import datetime


class Gallery(models.Model):
    name = models.CharField(max_length=100)
    pub_date = models.DateTimeField(default=datetime.now)
    private = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name_plural = "Galleries"

    def __str__(self):
        return f"{self.user.username}' gallery / {self.name} / {self.pub_date}"


class Image(models.Model):
    gallery = models.ForeignKey(Gallery, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="users")
    description = models.TextField(blank=True, null=True)
    pub_date = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return f"{self.gallery} / {self.pub_date}"
