from django import forms


class GalleryForm(forms.Form):
    name = forms.CharField(max_length=200)
    description = forms.CharField(widget=forms.Textarea, required=False)
    private = forms.BooleanField(widget=forms.CheckboxInput, required=False)
    front_image = forms.ImageField(required=False)

    #class Meta:



class ImageForm(forms.Form):
    image = forms.ImageField()
    description = forms.CharField(widget=forms.Textarea)